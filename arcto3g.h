/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id $
 *
 *    Revision history:
 *     $Log $
 *
 */

/*
 * arcto3g.h : Include file for arcto3g.c;
 */

#define MAX_STR			255
#define SHORT_STR		64
#define MEDIUM_STR		(SHORT_STR*2)
#define MAX_MAGDIST		10		/* max number of mag-distance entries */
#define MAX_MAND_STA	100		/* max number of mandatory SCN's */
#define MAX_STA			5000	/* the most stations we can handle */

/* Things read from config file
 ******************************/
char    OutputDir[SHORT_STR]; /* directory to write "triggers" to  */ 
char    TrigFileBase[SHORT_STR/2]; /* prefix of trigger file name  */

/* The big-deal data structures
*******************************/
typedef struct
{
	char		sta[TRACE_STA_LEN];
	char		comp[TRACE_CHAN_LEN];
	char		net[TRACE_NET_LEN];
	char		loc[TRACE_NET_LEN];
} SCNL_NAMES;

typedef struct
{
	int     	selected;
	double		startTime;
	double		Pat;
	double		duration;
        char            plabel;
        int iSta;
        char            presunct;
} SEL_STA;

/* Prototypes of functions in arcto3g.c
 *********************/
int sonOfSiteRead( char* filename );
int arcto3g_hinvarc( char* arcmsg, char* trigMsg, MSG_LOGO incoming_logo );
int phs2SelSta();
void  SelStaAddUnpicked();
int MagDist2SelSta( );
int SetTimesInSelSta();
void SortSelSta();
int SelSta2TrigMsg( char* trigMsg );
int SelSta2NmxpDoDRequest( char *NmxpDODFileBase, char *OutputDir);
double tau( double xmag );
int MandSta2Selected( );
int match( char* sta, char* comp, char* net, char* loc, EWDB_StationStruct* staStruct);
void arcto3g_config( char *configfile );
void arcto3g_lookup( void );
void arcto3g_status( unsigned char type, short ierr, char *note );
double Stime( double ot, double depth, double dist);
double Ptime( double ot, double depth, double dist);
int readTTval(double* val);
void bldtrig_hyp( char *trigmsg, MSG_LOGO incoming, MSG_LOGO outgoing);
