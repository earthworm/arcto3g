
CFLAGS = $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

SYSLIBS = -mt -lm -lsocket -lnsl -lposix4 -lc    

EWLIBS = $L/getutil.o $L/kom.o $L/transport.o $L/sleep_ew.o $L/dirops_ew.o $L/logit.o $L/time_ew.o \
	 $L/threads_ew.o $L/sema_ew.o $L/mem_circ_queue.o $L/chron3.o $L/read_arc.o \
	 $L/geo_to_km.o $L/make_triglist.o

PROGS = arcto3g

all: $(PROGS)
	cp $(PROGS) $B/

arcto3g: arcto3g.o $(EWLIBS)
	cc -o arcto3g arcto3g.o $(EWLIBS) $(SYSLIBS)

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<

lint:
	lint arcto3g.c $(CFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~ 

clean_bin:
	rm -f $(PROGS)
	cd $B; rm -f $(PROGS)
